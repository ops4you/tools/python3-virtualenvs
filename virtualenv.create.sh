#!/bin/bash
PURPOSE="create aportable python3 virtual environment using pip & virtualenv"

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# parse parameters
# -----------------------------------------------------------------------------

# usage
function printUsage(){
    echo "[i] PURPOSE: ${PURPOSE}"
    echo "[?] USAGE: ${scriptName} <ENV_NAME> [TEST_COMMAND]"
    exit 0
}
# params
[[ -z "${1}" ]] && printUsage
case "${1}" in
  # display usage help
  help|--help|-h)
    printUsage
  ;;
  *)
    [[ -n "${1}" ]] && ENV_NAME="${1}"
    [[ -n "${2}" ]] && TEST_COMMAND="${2}"
  ;;
esac


# script custom settings (customizable)
# -----------------------------------------------------------------------------

# work variables
ENV_NAME="${ENV_NAME:-virtualenv}"
TEST_COMMAND="${TEST_COMMAND:-which pip3}"
TMP_DIR="/tmp/virtualenvs"
OUT_DIR="${scriptFullDir}/releases"

# custom test scripts
function test_environment() {
  TEST_DIR="/tmp/test"
  [[ -d ${TEST_DIR} ]] || mkdir -p ${TEST_DIR}
  echo "[test] copying virtualenv to ${TEST_DIR}/${ENV_NAME}"
  [[ -d ${TEST_DIR}/${ENV_NAME} ]] && rm -rf ${TEST_DIR}/${ENV_NAME}
  cp -a ${TMP_DIR}/${ENV_NAME} ${TEST_DIR}/
  echo "[test] activating virtualenv ${TEST_DIR}/${ENV_NAME}"
  source ${TEST_DIR}/${ENV_NAME}/bin/activate
  echo "[test] displaying list of installed modules"
  pip3 list
  if [[ -n "${TEST_COMMAND}" ]];then
    echo "[test] running custom test command"
    ${TEST_COMMAND}
  fi
}


# ensure script is run as root user
# -----------------------------------------------------------------------------
if [[ $(id -u) -ne 0 ]];then
  echo "[x] ERROR: this script must be ran as root"
  exit 1
fi


# error handling
# -----------------------------------------------------------------------------
#set -e
reportErrors() { echo -e "\033[31m[x] ERROR: unexpected error occured on line [${1}]\033[0m"; exit 1; }
trap 'reportErrors ${LINENO}' ERR


# setup dependencies
# -----------------------------------------------------------------------------

# switch to script dir first
cd ${scriptFullDir} || exit 1

# helper function
installOsPackages(){
  local OS_PACKAGES="${*}"
  if apk --version >/dev/null 2>/dev/null;then 
    echo "[i] using apk package manager";  apk update && apk add ${OS_PACKAGES};
  elif apt --version >/dev/null 2>/dev/null;then 
    echo "[i] using apt package manager";  export DEBIAN_FRONTEND=noninteractive; apt-get update -qq && apt install -y ${OS_PACKAGES};
  elif dnf --version >/dev/null 2>/dev/null;then 
    echo "[i] using dnf package manager";  dnf install -y ${OS_PACKAGES};
  elif yum --version >/dev/null 2>/dev/null;then 
    echo "[i] using yum package manager";  yum install -y ${OS_PACKAGES};
  elif microdnf -h   >/dev/null 2>/dev/null;then 
    echo "[i] using µdnf package manager"; microdnf install -y ${OS_PACKAGES};
  else 
    echo "[x] no supported package manager found"; exit 1;
  fi
}

# read os informations
[[ -f /etc/os-release ]] && source /etc/os-release

# define os packages list
case $ID in
  # alpine specific
  alpine)
    OS_PACKAGES="build-base libffi-dev libc6-compat openssl-dev python3-dev py3-pip py3-virtualenv"
    ;;
  # by default
  *)
    OS_PACKAGES="python3-pip python3-virtualenv"
    # rhel7 specific packages
    if [[ ${ID} == "rhel" ]] && [[ "${VERSION%%.*}" == "7" ]];then
      echo "[>] using rh-packages on: ${PRETTY_NAME}"
      OS_PACKAGES="rh-python36-python-pip rh-python36-python-virtualenv"
      export PATH=/opt/rh/rh-python36/root/usr/bin:${PATH}
      export LANG=en_US.UTF-8
      export LC_ALL=en_US.UTF-8
      echo "[>] locales set to: ${LC_ALL}"
    fi
    ;;
esac

# install packages
echo "[i] current PATH value: ${PATH}"
echo "[>] installing packages: ${OS_PACKAGES}"
installOsPackages ${OS_PACKAGES}


# create virtualenv
# -----------------------------------------------------------------------------

# ensure TMP_DIR exists
[[ -d ${TMP_DIR} ]] || mkdir -p ${TMP_DIR}

# remove old (existing) virtualenv
[[ -d ${TMP_DIR}/${ENV_NAME} ]] && rm -rf ${TMP_DIR}/${ENV_NAME}

# create virtualenv
virtualenv ${TMP_DIR}/${ENV_NAME}

# copy custom files into virtualenv
CUSTOM_FILES="README.md requirements.txt virtualenv.*"
cp -v ${CUSTOM_FILES} ${TMP_DIR}/${ENV_NAME}/

# activate virtualenv
source ${TMP_DIR}/${ENV_NAME}/bin/activate

# update pip to latest version
pip3 install --upgrade pip

# install required pip packages
pip3 install -r ${TMP_DIR}/${ENV_NAME}/requirements.txt
pip3 list

# make virtualenv relocateable
virtualenv-make-relocatable ${TMP_DIR}/${ENV_NAME}
pushd ${TMP_DIR}/${ENV_NAME}/bin >/dev/null
cp -v activate activate.bak
echo '# compute current environment paths
sourcedFile=$BASH_SOURCE
sourcedFileDir=${sourcedFile%/*} 
sourcedFilePath=$(readlink -f ${sourcedFileDir})
sourcedTopDir=${sourcedFilePath%/*}
' >activate
sed 's/VIRTUAL_ENV=.*/VIRTUAL_ENV=${sourcedTopDir}/' activate.bak >>activate


# testing environment
# -----------------------------------------------------------------------------
test_environment


# create tgz release
# -----------------------------------------------------------------------------

# computing archive name
TGZ_NAME="${ENV_NAME}-${ID}-${VERSION_ID}.tgz"

# create release archive
[[ -d ${OUT_DIR} ]] || mkdir -p ${OUT_DIR}
tar czf ${OUT_DIR}/${TGZ_NAME} -C ${TMP_DIR} ${ENV_NAME}
echo "[done] ${ENV_NAME}-virtualenv has been exported to ${OUT_DIR}/${TGZ_NAME}"

# eof