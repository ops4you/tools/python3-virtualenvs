# python3-virtualenvs

## build

#### Description
this directory hosts a build environment for creating a ***"portable python3 environment"*** using:
  - the `bash` shell
  - the `python3 pip` package manager
  - the `python3 virtualenv` module
The build script will then create a tar.gz archive of the created environment.

#### `SUPPORTED OS PLATFORMS`
Builds has been tested on following OS releases/distributions:

  - **alpine**: Alpine Linux latest (python version 3.9.x)
  - **rhel7**: CentOS or RedHat Enterprise Linux 7.x (python version 3.6.x)
  - **rhel8**: CentOS or RedHat Enterprise Linux 8.x (python version 3.6.x)
  - **ubuntu**: Ubuntu LTS 20.04 (python version 3.8.x)


#### `REQUIREMENTS`
  - `root-privileges` on the build system
  - `internet and OS-repository access` from the build environment
  - according `python3`-version must be installed on target systems
  - `docker` + `docker-compose` on build-host if you want to use containers for building


#### `BUILDING on a build system`
  - please ensure that you have the same `python3`-version as the target systems
  ```
  python3 --version
  ```

  - run the virtualenv build-script (use --help for usage help)
  ```
  ./virtualenv.create.sh <ENV_NAME>
  ```
> If your server has no direct internet access, you should define the use of a proxy server by setting the env vars `http_proxy` and `https_proxy`: `export https_proxy="http://my.proxy.host:8080"`



#### `BUILDING using a container image`
  - please ensure that the **image** used have the same `python3`-version as the target systems
  - run the virtualenv build-script (use --help for usage help)
  ```
  # using docker-compose
  export ENV_NAME=ansible-latest# ensure you define a short name for the environment
  docker-compose run --rm <BUILD-SERVICE> 

  # docker-compose examples
  docker-compose run --rm build-rhel8 
  docker-compose run --rm build-ubuntu 

  # using docker/podman
  docker run --rm -it -v $PWD:/build <IMAGE> /build/virtualenv.create.sh <ENV_NAME>
  
  # on alpine, bash must be installed first
  docker run --rm -it -v $PWD:/build alpine:latest sh -c 'apk add --no-cache bash && /build/virtualenv.create.sh <ENV_NAME>'
  ```
> If your server has no direct internet access, you should define the use of a proxy server by setting the env vars `http_proxy` and `https_proxy` (adding the -e option): `-e https_proxy="http://my.proxy.host:8080"`



## usage

#### `REQUIREMENTS`
  - the ansible virtual environment relies on an existing `python3` installation.
  - you **DO NOT** need `root-privileges` to run this environment (ex: ansible can be installed in user's home directory).

#### `INSTALLATION`
  - Download and unpack the corresponding archive file:
  ```
  tar xvzf <release-file>.tgz
  ```

  - This will create a directory named like the `ENV_NAME` used on build in the current folder that you could use immediately by using full paths (ex with ENV_NAME=ansible):
  ```
  #> ./ansible/bin/ansible localhost -a 'echo "Hello World'
  ```

  - But if you want a full integration into the current environment, `activate` the virtualenv first. You will then be able to run all ansible commands directly:
  ```
  #> source ./ansible/bin/activate
  (ansible) #> ansible --version
  ```
  Just type `deactivate`if you want to leave the virtual environment
  ```
  (ansible) #> deactivate
  #>
  ```
